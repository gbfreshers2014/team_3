package com.gwynniebee.inventoryManagement.Entities;


import java.util.List;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.dao.InventoryDAO;
import com.gwynniebee.inventoryManagement.dao.mapper.InventoryMapper;
import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.inventoryManagement.object.Inventory;




public class InventoryEntityManager extends BaseEntityManager {

    private static final Logger LOG = LoggerFactory.getLogger(InventoryEntityManager.class);
    private static final InventoryEntityManager INSTANCE = new InventoryEntityManager();

    /**
     * Default constructor.
     */
    public InventoryEntityManager() {
    }

    /**
     * Return the instance for InventoryEntityManager.
     * @return instance
     */
    public static InventoryEntityManager getInstance() {
        return INSTANCE;
    }

    /**
     * Add inventory into database.
     * @param inventory {@link inventory} object to add
     * @return return updated inventory after adding to database
     * @throws ClassNotFoundException
     */
    public Inventory addInventory(Inventory inventory) throws ClassNotFoundException {
        InventoryDAO dao = null;

        Handle h = null;
        try {
                      
            h = getDbi().open();
            LOG.debug("connected to database successfully.");
            h.begin();
            inventory.setStatus(1);
            dao = h.attach(InventoryDAO.class);
            inventory.setItemId(dao.addInventory(inventory));
           
            LOG.debug("Added inventory successfully." + inventory);
            h.commit();
           inventory = this.getInventoryByItemID(inventory.getItemId());
           
        } catch (RuntimeException e) {
            if (h != null) {
                h.rollback();
            }

            LOG.error("Error while adding inventory." + inventory.toString() + ", Exception: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (h != null) {
                h.close();
            }
        }

        return inventory;
    }
    public Inventory getInventoryByItemID(int itemID){
        
        
        InventoryDAO dao = null;
        Inventory inventory = new Inventory();
       
        try {
            
            dao = getDbi().open(InventoryDAO.class);           
            LOG.debug("inside getInventoryByItemID.");         
           
            inventory = dao.getInventoryByItemID(itemID);
           
            LOG.debug("Got inventory : " + inventory.toString());

        }  finally {
            if (dao != null) {
                dao.close();
            }
        }
        return inventory;
        
    }

    public List<Inventory> getInventory(FilterParams filterParams) {
        // TODO Auto-generated method stub
        List<Inventory> inventoryList ;
        Handle h = null;
        try {
        String query = filterParams.buildQuery();
        LOG.info("Query -> " + query);
        h = getDbi().open();           
        LOG.debug("inside getINventory with filtering.");         
       
        inventoryList = h.createQuery(query).map(new InventoryMapper()).list();
        LOG.debug("Got inventory : " + inventoryList.toString());
       
        }finally {
            if (h != null) {
                h.close();
            }
        }
        
        return inventoryList;
    }

}
