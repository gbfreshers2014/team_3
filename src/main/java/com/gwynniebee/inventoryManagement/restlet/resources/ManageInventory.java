package com.gwynniebee.inventoryManagement.restlet.resources;

import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.Entities.InventoryEntityManager;
import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryResponse;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryListResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;



/*
 * 
 * */
public class ManageInventory extends AbstractServerResource {
    
    private static final Logger LOG = LoggerFactory.getLogger(ManageInventory.class);
    
    @Post
    public InventoryResponse addInventory(Inventory inventory){
        
        InventoryResponse response = new InventoryResponse();
        
        if(inventory == null)
        {            
            this.getRespStatus().setMessage("Empty input inventory is not allowed.");
            this.getRespStatus().setCode(ResponseStatus.ERR_CODE_INVALID_REQ);
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            response.setStatus(this.getRespStatus());
            LOG.info("AddInventory: null inventory received. No add done.");
            
        }else{
            
            LOG.info("Request : " + inventory.toString());
            try {
                
                
                Inventory dbInventory = InventoryEntityManager.getInstance().addInventory(inventory);
                
                getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
                getRespStatus().setMessage("success");
               
                setStatus(Status.SUCCESS_OK);
                response.setInventory(dbInventory);
                response.setStatus(getRespStatus());
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
           
            
        }
       
        return response;
        
    }
    
    @Get
    public InventoryListResponse getInventory(){
        
        LOG.info("Request to get all the inventory");
        InventoryListResponse response = new InventoryListResponse();
        List<Inventory> inventorys = null;
        FilterParams filterParams = ServerResourceUtils.prepareGetParams(this);
        inventorys =  InventoryEntityManager.getInstance().getInventory(filterParams);
        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("success"); 
        setStatus(Status.SUCCESS_OK);
        response.setInventorys(inventorys);
        response.setStatus(getRespStatus());   
       
        return response;
    }

}
