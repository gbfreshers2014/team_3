package com.gwynniebee.inventoryManagement.restlet.resources;

import org.restlet.data.Form;

import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class ServerResourceUtils {

    public static FilterParams prepareGetParams(AbstractServerResource serverResource) {

        FilterParams filterParams = new FilterParams();

        String itemName = getParamValue(serverResource, "itemName");
        if ((itemName != null) && (itemName.length() > 0)) {
            filterParams.addCriteria("itemName", itemName);
        }

        String status = getParamValue(serverResource, "status");
        if ((status != null) && (status.length() > 0)) {
            filterParams.addCriteria("status", status);
        }

        String itemType = getParamValue(serverResource, "itemType");
        if ((itemType != null) && (itemType.length() > 0)) {
            filterParams.addCriteria("itemType", itemType);
        }
        String itemId = getParamValue(serverResource, "itemId");
        if ((itemId != null) && (itemId.length() > 0)) {
            filterParams.addCriteria("itemId", itemId);
        }
        return filterParams;

    }

    private static String getParamValue(AbstractServerResource serverResource, String key) {
        Form queryParams = serverResource.getRequest().getResourceRef().getQueryAsForm();
        String value = queryParams.getFirstValue(key);
        return value;
    }

}
