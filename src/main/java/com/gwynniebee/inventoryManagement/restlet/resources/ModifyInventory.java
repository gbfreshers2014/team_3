package com.gwynniebee.inventoryManagement.restlet.resources;


import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.Entities.InventoryEntityManager;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;


public class ModifyInventory extends AbstractServerResource{
    
    private static final Logger LOG = LoggerFactory.getLogger(ModifyInventory.class);   
    
    @Get
    public InventoryResponse getInventoryByItemId()  {
        
        LOG.info("Request to get modify inventory");
        String itemId = (String) getRequest().getAttributes().get("itemId");
        InventoryResponse modifyresponse = new InventoryResponse();
        Inventory inventory;
        
        //code ==> validation for itemId
        
        inventory =  InventoryEntityManager.getInstance().getInventoryByItemID(Integer.parseInt(itemId));
        
        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("success"); 
        setStatus(Status.SUCCESS_OK);
        
        modifyresponse.setInventory(inventory);
        
        modifyresponse.setStatus(getRespStatus()); 
        
        
        return modifyresponse;
    }

}
