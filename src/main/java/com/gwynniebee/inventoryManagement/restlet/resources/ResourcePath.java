package com.gwynniebee.inventoryManagement.restlet.resources;

public class ResourcePath {
    
    public static final String URL_INVENTORY_LIST_RESOURCE = "/v1/t3/inventory/inventoryItems.json";
    public static final String URL_MODIFY_INVENTORY_RESOURCE = "/v1/t3/inventory/inventoryItems/%s";
    public static final String URL_TRANSACTION_INVENTORY_RESOURCE = "/v1/t3/inventory/transactions.json";
   
    
   
    /**
    * Get inventory list resource path.
    * @return v1/t3/inventory/inventoryItems.json
    */
    
   public static String getInventoryListResourcePath() {
       return URL_INVENTORY_LIST_RESOURCE;
   }
   
   /**
    * Get modify inventory resource path.
    * @param itemId itemId
    * @return v1/t3/inventory/inventoryItems/{itemId}.json
    */
   public static String getModifyInventoryResourcePath(String itemId) {
       return String.format(URL_MODIFY_INVENTORY_RESOURCE, itemId);
   }

}
