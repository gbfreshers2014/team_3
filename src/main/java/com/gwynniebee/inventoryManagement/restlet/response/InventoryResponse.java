package com.gwynniebee.inventoryManagement.restlet.response;


import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.rest.common.response.AbstractResponse;



public class InventoryResponse extends AbstractResponse {
    //public String str = "ADDED";
    
    private Inventory inventory;

    /**
     * Default constructor.
     */
    public InventoryResponse() {

    }

    /**
     * @return the shipments
     */
    public Inventory getInventory() {
        return this.inventory;
    }

    /**
     * @param shipments the shipments to set
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InventoryAddResponse [Inventory=" + this.inventory + "]";
    }

   
}
