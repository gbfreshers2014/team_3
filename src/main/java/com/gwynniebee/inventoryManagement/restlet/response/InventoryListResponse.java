package com.gwynniebee.inventoryManagement.restlet.response;

import java.util.List;

import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.inventoryManagement.object.Inventory;

public class InventoryListResponse extends AbstractResponse{

    private List<Inventory> inventorys;

    /**
     * @return the inventorys
     */
    public List<Inventory> getInventorys() {
        return this.inventorys;
    }

    /**
     * @param inventorys the inventorys to set
     */
    public void setInventorys(List<Inventory> inventorys) {
        this.inventorys = inventorys;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InventoryListResponse [inventorys=" + this.inventorys + "]";
    }

    
    
    
}
