/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.restlet;

import java.util.Properties;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;
import com.gwynniebee.inventoryManagement.Entities.BaseEntityManager;
import com.gwynniebee.inventoryManagement.constants.DBConstants;
import com.gwynniebee.inventoryManagement.constants.InventoryManagementConstants;
import com.gwynniebee.inventoryManagement.restlet.resources.ManageInventory;
import com.gwynniebee.inventoryManagement.restlet.resources.Experimentnventory;
import com.gwynniebee.inventoryManagement.restlet.resources.ModifyInventory;
import com.gwynniebee.inventoryManagement.restlet.resources.ResourcePath;

/**
 * Barcode file upload service routing and controlling resources.
 * @author Sarath
 */

public class InventoryManagementService extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(InventoryManagementService.class);
    private static Validator validator;
    private Properties serviceProperties;

    /**
     * (From Application.getCurrent)<br>
     * This variable is stored internally as a thread local variable and updated
     * each time a call enters an application.<br>
     * <br>
     * Warning: this method should only be used under duress. You should by
     * default prefer obtaining the current application using methods such as
     * {@link org.restlet.resource.Resource#getApplication()} <br>
     * @return The current application.
     */
    public static InventoryManagementService getCurrent() {
        return (InventoryManagementService) Application.getCurrent();
    }

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        resourceUrl = "/hello-world.json";
        router.attach(resourceUrl, Experimentnventory.class);
        
        
        resourceUrl = ResourcePath.getInventoryListResourcePath();
        
        router.attach(resourceUrl, ManageInventory.class);
        
       // resourceUrl = ResourcePath.getModifyInventoryResourcePath(InventoryManagementConstants.ITEMID);   
        resourceUrl = "/v1/t3/inventory/inventoryItems/{itemId}";
        LOG.info("search by itemID "+resourceUrl);  
        router.attach(resourceUrl, ModifyInventory.class);
        
        return router;
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#start()
     */
    @Override
    public synchronized void start() throws Exception {
        if (!this.isStarted()) {

            // Prepare Properties
            this.serviceProperties = IOGBUtils.getPropertiesFromResource("/inventory.management.service.properties");
            
            //Establish DataBase connection
            Class.forName(DBConstants.DATABASE_DRIVER);
            DBI dbi = new DBI(DBConstants.DATABASE_INVENTORY_URL, DBConstants.DATABASE_USERNAME, DBConstants.DATABASE_PASSWORD);
            BaseEntityManager.setDbi(dbi);
            // Prepare validation factory
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            InventoryManagementService.validator = factory.getValidator();
        }
        // below will make this.isStarted() true
        super.start();
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }
}
