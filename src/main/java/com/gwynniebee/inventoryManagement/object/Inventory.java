package com.gwynniebee.inventoryManagement.object;

import org.joda.time.DateTime;

public class Inventory {

    /**
     * Auto increment primary key for shipment.
     */
    private int itemId; 
    
    /**
     * Mandatory<br>
     * Cannot be null.
     */
    private String itemType;
    
    /**
     * Mandatory<br>
     * Cannot be null.
     */    
    private String itemName;
   
    /**
     * Description of item.<br>
     * Optional.
     */    
    private String description;
    
    /**
     * Admin name who added the current item.<br>
     * cannot be null.<br>
     * User cannot provide this value. It is generated by server..
     */    
    private String createdBy;
    
    /**
     * Date of creation.<br>
     * User cannot provide this value. It is generated by server..
     */
    private DateTime createdOn;
    

    /**
     * Status of the item.<br>
     * it can be Available =1 or not_Available = 2 or Deleted = 3. <br>
     */
    private int status;
    
    
    /**
     * Return nameSpaceID of issued item.<br>
     * User can provide this value.
     */
    private int namespaceId;
    
    /**
     * Admin ID who updated the item.<br>
     * User cannot provide this value. It is generated by server..
     */
    private String updatedBy;
    
    /**
     * Date of updation.<br>
     * User cannot provide this value. It is generated by server..
     */
    private DateTime updatedOn;
    
    
    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    

    public int getNamespaceId() {
        return namespaceId;
    }

    public void setNamespaceId(int namespaceId) {
        this.namespaceId = namespaceId;
    }

    public DateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public DateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(DateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return status+" : "+itemName + " : " + itemType + " : " + createdBy + " : " + description+" : "+ namespaceId+" : "+createdBy+" : "+createdOn+" : "+updatedBy +" : "+updatedOn;
    }

}
