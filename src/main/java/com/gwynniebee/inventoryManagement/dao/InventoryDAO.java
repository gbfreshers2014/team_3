package com.gwynniebee.inventoryManagement.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryManagement.dao.mapper.InventoryMapper;
import com.gwynniebee.inventoryManagement.object.Inventory;




/**
 * Database access dao for inventory.
 * @author Ipsita
 */
@RegisterMapper(InventoryMapper.class)
public interface InventoryDAO extends Transactional<InventoryDAO>{    
    
  /*  CREATE TABLE `INVENTORY` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(50) NOT NULL,
  `itemType` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `namespaceId` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(254) NOT NULL,
  `updatedBy` varchar(254) NOT NULL,
  `updatedOn` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemId`),
  KEY `STATUS` (`status`),
  CONSTRAINT `fk_INVENTORY_1` FOREIGN KEY (`status`) REFERENCES `INVENTORY_STATUS` (`statusNumber`) ON UPDATE CASCADE
);
    */
    /**
     * Add inventory into database.
     * @param inventory {@link inventory} object to add
     * @return return auto generated id of inventory.
     */
    @SqlUpdate("insert into INVENTORY " + "( itemName, itemType, description, namespaceId, createdBy, "
            + " updatedBy,  status) values (:i.itemName, :i.itemType,"
            + " :i.description, :i.namespaceId, :i.createdBy, :i.updatedBy,  :i.status)")
    @GetGeneratedKeys
    int addInventory(@BindBean("i") Inventory inventory);
    
    
    /**
     * Get Inventory object by ItemID.
     * @param id id of inventory
     * @return {@link Inventory} object
     */
    @SqlQuery("select itemId, itemName, itemType, description, namespaceId, createdOn, createdBy, "
            + " updatedBy, updatedOn,  status from INVENTORY where itemId = :id")
    Inventory getInventoryByItemID(@Bind("id") int id);

    /**
     * closes the underlying connection.
     */
    void close();
}
