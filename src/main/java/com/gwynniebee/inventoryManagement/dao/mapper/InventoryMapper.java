package com.gwynniebee.inventoryManagement.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryManagement.object.Inventory;

public class InventoryMapper implements ResultSetMapper<Inventory> {

    @Override
    public Inventory map(int arg0, ResultSet arg, StatementContext arg2) throws SQLException {
        // TODO Auto-generated method stub
        
        Inventory inventory = new Inventory();
        ResultSetMetaData metadata = arg.getMetaData();
      
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {

            String name = metadata.getColumnLabel(i);

            if (name.equalsIgnoreCase("itemid")) {
                inventory.setItemId(arg.getInt("itemId"));
            } else if (name.equalsIgnoreCase("itemName")) {
                inventory.setItemName(arg.getString("itemName"));
            } else if (name.equalsIgnoreCase("itemType")) {
                inventory.setItemType(arg.getString("itemType"));
            } else if (name.equalsIgnoreCase("description")) {
                inventory.setDescription(arg.getString("description"));
            } else if (name.equalsIgnoreCase("namespaceId")) {
                inventory.setNamespaceId(arg.getInt("namespaceId"));
            }  else if (name.equalsIgnoreCase("createdOn")) {
                Timestamp timestamp = arg.getTimestamp("createdOn");
                if (timestamp != null) {
                    inventory.setCreatedOn(new DateTime(timestamp.getTime(), DateTimeZone.UTC));
                }
            }  else if (name.equalsIgnoreCase("createdBy")) {
                inventory.setCreatedBy(arg.getString("createdBy"));
            }else if (name.equalsIgnoreCase("updatedOn")) {
                Timestamp timestamp = arg.getTimestamp("updatedOn");
                if (timestamp != null) {
                    inventory.setUpdatedOn(new DateTime(timestamp.getTime(), DateTimeZone.UTC));
                }
            }else if (name.equalsIgnoreCase("updatedBy")) {
                inventory.setStatus(arg.getInt("updatedBy"));
            }  else if (name.equalsIgnoreCase("status")) {
                inventory.setStatus(arg.getInt("status"));
            } 
        }
  
        return inventory;
        
    }

}
