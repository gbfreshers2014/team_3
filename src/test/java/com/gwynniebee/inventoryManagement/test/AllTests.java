/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.gwynniebee.inventoryManagement.test.serverresource.AllServerResourceTest;

/**
 * All test cases.
 * @author Jitender
 */
@RunWith(Suite.class)
@SuiteClasses({AllServerResourceTest.class})
public class AllTests {

}
