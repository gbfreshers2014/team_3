package com.gwynniebee.inventoryManagement.test;


import org.junit.Test;

import com.gwynniebee.inventoryManagement.Entities.InventoryEntityManager;
import com.gwynniebee.inventoryManagement.object.Inventory;



public class AddINventory {
    
    /* "itemName" : "dell",
        "itemType" : "laptop",
        "description" : "can be NULL",
        "namespaceId" : 12,
        "createdOn"  : "",
         "createdBy" : "1",
        "updatedBy" : "1"
     * */
    @Test
    public void addInventoryTest() throws ClassNotFoundException{
        Inventory inv = new Inventory();
        
        inv.setItemName("dell");
        inv.setItemType("laptop");        
        inv.setNamespaceId(1);
        inv.setCreatedBy("1");        
        inv.setUpdatedBy("1");
        InventoryEntityManager i = new InventoryEntityManager();
      i.addInventory(inv);
        
       
        
    }

}
